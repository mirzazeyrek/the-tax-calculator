<?php

namespace App\Http\Controllers;

use App\Country;
use App\State;
use App\TaxPayer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{

    /**
     * Lists the all tax payers if pagination param is missing.
     *
     * @param Request $request
     * @return Contact[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        DB::enableQueryLog();
        $model = TaxPayer::with('county','state', 'county.state.country');
        // Pagination only if requested.
        if ($request->has('page') && is_numeric($request->input('page'))) {
            $skip = 10*($request->input('page') - 1);
            $model->skip($skip);
            $model->limit(100);
        }
        $data['tax_payers'] = $model->get();

        return view('taxpayers', $data);
    }

    /**
     * Lists the all tax payers if pagination param is missing.
     *
     * @param Request $request
     * @return Contact[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function states(Request $request)
    {
        DB::enableQueryLog();
        $model = Country::with('states');
        // Pagination only if requested.
        if ($request->has('page') && is_numeric($request->input('page'))) {
            $skip = 10*($request->input('page') - 1);
            $model->skip($skip);
            $model->limit(100);
        }
        $data['countries'] = $model->get();

        return view('states', $data);
    }

    /**
     * Lists the all tax payers if pagination param is missing.
     *
     * @param Request $request
     * @return Contact[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function countries(Request $request)
    {
        DB::enableQueryLog();
        $model = Country::with('states');
        // Pagination only if requested.
        if ($request->has('page') && is_numeric($request->input('page'))) {
            $skip = 10*($request->input('page') - 1);
            $model->skip($skip);
            $model->limit(100);
        }
        $data['countries'] = $model->get();

        return view('countries', $data);
    }

    public function refreshdb()
    {
        Artisan::call('migrate:refresh', ['--seed' => '1']);
        return redirect('/');
    }
}
