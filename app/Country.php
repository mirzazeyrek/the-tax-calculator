<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $timestamps = false;

    /**
     * Get the tax states for the country.
     */
    public function states()
    {
        return $this->hasMany('App\State', 'country_id', 'id');
    }

    public function tax_payers()
    {
        $tax_payers = new Collection();
        foreach( $this->states()->get() as $state ) {
            $tax_payers = $tax_payers->merge( $state->tax_payers()->get() );
        }
        return $tax_payers;
    }

    public function tax_rates()
    {
        $tax_rates = new Collection();
        foreach( $this->states()->get() as $rate ) {
            $tax_rates = $tax_rates->merge( $rate->tax_rates()->get() );
        }
        return $tax_rates;
    }

    public function getOverallTaxAttribute() {
        $tax_payers = $this->tax_payers();
        $sum = 0;
        foreach( $tax_payers as $tax_payer ) {
            $sum += ( $tax_payer->tax_rate->rate * $tax_payer->income ) / 100;
        }
        return $sum;
    }

    public function getTotalIncomeAttribute() {
        return $this->tax_payers()->sum('income');
    }

    public function getTaxPayerCountAttribute() {
        return $this->tax_payers()->count();
    }

    public function getAvgTaxPerPersonAttribute() {
        return $this->getOverallTaxAttribute()/$this->getTaxPayerCountAttribute();
    }

    public function getAvgCountyTaxRateAttribute() {
        return $this->tax_rates()->avg('rate');
    }
}
