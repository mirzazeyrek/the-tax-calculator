<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public $timestamps = false;

    /**
     * Get the tax states for the country.
     */
    public function counties()
    {
        return $this->hasMany('App\County', 'state_id', 'id');
    }

    public function country() {
        return $this->belongsTo('App\Country');
    }

    public function tax_rates() {
        return $this->hasManyThrough('App\TaxRate', 'App\County');
    }

    public function getAvgCountyTaxRateAttribute() {
        return $this->tax_rates()->avg('rate');
    }

    public function tax_payers() {
        return $this->hasManyThrough('App\TaxPayer', 'App\County');
    }

    public function getTotalIncomeAttribute() {
        return $this->tax_payers()->sum('income');
    }

    public function getOverallTaxAttribute() {
        $tax_payers = $this->tax_payers()->get();
        $sum = 0;
        foreach( $tax_payers as $tax_payer ) {
            $sum += ( $tax_payer->tax_rate->rate * $tax_payer->income ) / 100;
        }
        return $sum;
    }

    public function getTaxPayerCountAttribute() {
        return $this->tax_payers()->count();
    }

    public function getAvgTaxPerPersonAttribute() {
        return $this->getOverallTaxAttribute()/$this->getTaxPayerCountAttribute();
    }
}
