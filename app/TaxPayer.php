<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxPayer extends Model
{
    public $timestamps = false;

    public function county() {
        return $this->belongsTo('App\County');
    }


    public function state()
    {
        return $this->belongsTo('App\County');
    }

    public function tax_rates()
    {

        return $this->hasMany('App\TaxRate', 'county_id', 'county_id');
    }


    public function tax_rate()
    {
        return $this->hasOne( 'App\TaxRate', 'county_id', 'county_id' )
            ->where('above', '<', $this->income )
            ->orderBy('above', 'desc');
    }

    public function getTaxAttribute(){
        return  $this->tax_rate->rate*$this->income/100;
    }

    public function getNetIncomeAttribute(){
        return   $this->income-$this->tax;
    }

    public function getStateNameAttribute(){
        return  $this->county->state->name;
    }

    public function getCountryNameAttribute(){
        return  $this->county->state->country->name;
    }

    public function net_income() {
        return ( $this->income-$this->tax()->rate );
    }
}
