<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    public $timestamps = false;

    /**
     * Get the tax rates for the county.
     */
    public function tax_rates()
    {
        return $this->hasMany('App\TaxRate', 'county_id', 'id');
    }

    /**
     * Get the tax rates for the county.
     */
    public function tax_payer()
    {
        return $this->hasMany('App\TaxPayer', 'county_id', 'id');
    }

    public function state() {
        return $this->belongsTo('App\State');
    }

}
