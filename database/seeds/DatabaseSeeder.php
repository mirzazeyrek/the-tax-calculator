<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         //factory('App\TaxRate', 10)->states('highest' )->create();
        $multiplier = 5;
        factory('App\Country', $multiplier )->create()->each(function ($country) use ( $multiplier ) {
            for($i=0; $i<$multiplier; $i++) {
                $state = $country->states()->save( factory( App\State::class )->create( [ 'country_id' => $country->id ] ) );
                for( $ii=0; $ii<$multiplier; $ii++ ) {
                    $county = $state->counties()->save( factory( App\County::class )->create( [ 'state_id' => $state->id ] ) );

                    // Create thresholds for taxation.
                    $county->tax_rates()->save(factory(App\TaxRate::class)->states('minimum')->create( ['county_id' => $county->id ] ) );
                    $county->tax_rates()->save(factory(App\TaxRate::class)->states('medium')->create( ['county_id' => $county->id ] ) );
                    $county->tax_rates()->save(factory(App\TaxRate::class)->states('high')->create( ['county_id' => $county->id ] ) );
                    $county->tax_rates()->save(factory(App\TaxRate::class)->states('highest')->create( ['county_id' => $county->id ] ) );

                    // Create tax payers for each threshold.
                    $county->tax_payer()->save(factory(App\TaxPayer::class)->states('minimum')->create( ['county_id' => $county->id ] ) );
                    $county->tax_payer()->save(factory(App\TaxPayer::class)->states('medium')->create( ['county_id' => $county->id ] ) );
                    $county->tax_payer()->save(factory(App\TaxPayer::class)->states('high')->create( ['county_id' => $county->id ] ) );
                    $county->tax_payer()->save(factory(App\TaxPayer::class)->states('highest')->create( ['county_id' => $county->id ] ) );
                }
            }
        });
    }
}
