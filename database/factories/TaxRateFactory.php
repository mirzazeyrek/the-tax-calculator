<?php
use Faker\Generator as Faker;
// Don't use it directly. Call ClientFactory for creating attached models.
$factory->define(App\TaxRate::class, function (Faker $faker, $attributes ) {
    return [
        'county_id' =>  $attributes['county_id'] ?? factory(\App\County::class)->create()->id,
        'rate' => $attributes['rate'] ?? $faker->randomFloat(2, 1, 10 ),
        'below' => $attributes['below'] ?? $faker->randomFloat(2, 10, 100),
        'above' => $attributes['above'] ?? $faker->randomFloat(2, 10, 100),
    ];
});

$factory->state(App\TaxRate::class, 'minimum', function ($faker) {
    return [
        'rate' => $attributes['rate'] ?? $faker->randomFloat(2, 1, 10 ),
        'below' => $attributes['below'] ?? 12000,
        'above' => $attributes['above'] ?? 0,
    ];
});

$factory->state(App\TaxRate::class, 'medium', function ($faker) {
    return [
        'rate' => $attributes['rate'] ?? $faker->randomFloat(2, 10, 20 ),
        'below' => $attributes['below'] ?? 36000,
        'above' => $attributes['above'] ?? 12000,
    ];
});

$factory->state(App\TaxRate::class, 'high', function ($faker) {
    return [
        'rate' => $attributes['rate'] ?? $faker->randomFloat(2, 20, 30 ),
        'below' => $attributes['below'] ?? 80000,
        'above' => $attributes['above'] ?? 36000,
    ];
});

$factory->state(App\TaxRate::class, 'highest', function ($faker) {
    return [
        'rate' => $attributes['rate'] ?? $faker->randomFloat(2, 30, 40 ),
        'below' => $attributes['below'] ?? 0,
        'above' => $attributes['above'] ?? 80000,
    ];
});
