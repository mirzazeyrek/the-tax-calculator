<?php
use Faker\Generator as Faker;

$factory->define(App\County::class, function (Faker $faker, $attributes ) {
    return [
        'name' =>  $attributes['county'] ?? $faker->unique()->streetName,
        'state_id'=>  $attributes['state_id'] ?? factory(\App\State::class)->create()->id,
    ];
});

