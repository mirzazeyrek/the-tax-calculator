<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(App\Country::class, function (Faker $faker, $attributes) {
    return [
        'name' =>  $attributes['country'] ?? $faker->unique()->country,
    ];
});
