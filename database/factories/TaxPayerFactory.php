<?php
use Faker\Generator as Faker;
// Don't use it directly. Call ClientFactory for creating attached models.
$factory->define(App\TaxPayer::class, function (Faker $faker, $attributes ) {
    return [
        'county_id' =>  $attributes['county_id'] ?? factory(\App\County::class)->create()->id,
        'name' => $attributes['name'] ?? $faker->name,
        'income' => $attributes['below'] ?? $faker->randomFloat(2, 1000, 1000000),
    ];
});

$factory->state(App\TaxPayer::class, 'minimum', function ($faker) {
    return [
        'income' => $attributes['income'] ?? $faker->randomFloat(2, 1, 12000 ),
    ];
});

$factory->state(App\TaxPayer::class, 'medium', function ($faker) {
    return [
        'income' => $attributes['income'] ?? $faker->randomFloat(2, 12000, 36000 ),
    ];
});

$factory->state(App\TaxPayer::class, 'high', function ($faker) {
    return [
        'income' => $attributes['income'] ?? $faker->randomFloat(2, 36000, 80000 ),
    ];
});

$factory->state(App\TaxPayer::class, 'highest', function ($faker) {
    return [
        'income' => $attributes['income'] ?? $faker->randomFloat(2, 80000, 1000000 ),
    ];
});
