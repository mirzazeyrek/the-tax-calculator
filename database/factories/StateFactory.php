<?php
use Faker\Generator as Faker;
// Don't use it directly. Call ClientFactory for creating attached models.
$factory->define(App\State::class, function (Faker $faker, $attributes ) {
    return [
        'name' =>  $attributes['state'] ?? $faker->unique()->city,
        'country_id'=>  $attributes['country_id'] ?? factory(\App\Country::class)->create()->id,
    ];
});

