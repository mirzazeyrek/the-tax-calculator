<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                @include('navmenu')
                <div class="title m-b-md">
                    The Tax Calculator - Tax Payers
                </div>
                <div class="text-body">
                    @if (count($tax_payers))
                        <table border="1" cellspacing="1" cellpadding="5">
                            <tr>
                                <th> ID </th>
                                <th> NAME </th>
                                <th> INCOME </th>
                                <th> NET INCOME </th>
                                <th> TAX </th>
                                <th> TAX RATE </th>
                                <th> TAX CATEGORY </th>
                                <th> TAX ID </th>
                                <th> COUNTY </th>
                                <th> STATE </th>
                                <th> COUNTRY </th>
                            </tr>
                        @foreach($tax_payers as $tax_payer)
                            <tr>
                                <td>{{ $tax_payer->id }}</td>
                                <td>{{ $tax_payer->name }}</td>
                                <td>{{ number_format( $tax_payer->income, 2 ) }}</td>
                                <td>{{ number_format( $tax_payer->net_income, 2 ) }} </td>
                                <td>{{ number_format( $tax_payer->tax, 2 ) }}</td>
                                <td>{{ $tax_payer->tax_rate->rate }}</td>
                                <td align="center"> < €{{ number_format( $tax_payer->tax_rate->below ) }} <br /> > €{{ number_format( $tax_payer->tax_rate->above ) }} </td>
                                <td>{{ $tax_payer->tax_rate->id }}</td>
                                <td>{{ $tax_payer->county->name }}</td>
                                <td>{{ $tax_payer->state_name }}</td>
                                <td>{{ $tax_payer->country_name }}</td>

                            </tr>
                        @endforeach
                        </table>
                    @else
                        <p>No Tax Payers.</p>
                    @endif
                </div>
            </div>
        </div>
    </body>
</html>
